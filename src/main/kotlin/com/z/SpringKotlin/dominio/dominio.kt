package com.z.SpringKotlin.dominio

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
//Con @Entity JPA puede saber que esto sera tratado como una tabla
@Entity
class Producto() {
    //Neceario para dar el valor unico a la tabla
    @Id
    // Crea automanticamente el valor identity auto incremental
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id:Int = 0
    @get:NotBlank
    var producto:String = ""
    @get:Min(2)
    var precio:Int = 0

    constructor(producto: String, precio: Int):this()
    {
        this.id=id
        this.producto= producto
        this.precio= precio
    }
}