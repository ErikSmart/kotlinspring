package com.z.SpringKotlin.controller

import com.z.SpringKotlin.dominio.Producto
import com.z.SpringKotlin.servicios.ProductoServicio
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/")
class InicioController(private val productoServicio: ProductoServicio){
	/*Inyecion del contructor por inicializacion retrazada
	*
	@Autowired
	private lateinit var productoServicio: ProductoServicio

	* */
	@GetMapping("/")
	fun findAll() = productoServicio.findAll()
	//GetMapping es el enretulador
	@GetMapping("/producto/{id}")
	fun findById(@PathVariable id:String): ResponseEntity<Producto> {
		val entity= productoServicio.findById(id)
		return ResponseEntity.status(if (entity!=null){
			HttpStatus.OK
		} else  HttpStatus.NOT_FOUND).body(entity)

	}
	//Metodo post
	@PostMapping("/")
	// @Valid es para poder restringir los tipos de datos del domino o Modelo Ejemplo: @get:NotBlank
	fun guardar(@Valid @RequestBody producto: Producto): ResponseEntity<Boolean> {
		val entity= productoServicio.guardar(producto)
		return ResponseEntity.status(if (entity) {HttpStatus.CREATED }else {HttpStatus.NO_CONTENT}).body(entity)
	}
	//Metodo Put
	@PutMapping("/")
	fun actualizar(@RequestBody producto: Producto) = productoServicio.actualizar(producto)
	@DeleteMapping("/{id}")
	fun eliminar(@PathVariable id:String) = productoServicio.eliminar(id)

}