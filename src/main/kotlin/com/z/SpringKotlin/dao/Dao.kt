package com.z.SpringKotlin.dao

import com.z.SpringKotlin.dominio.Producto
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductoDAO: JpaRepository<Producto,String>

