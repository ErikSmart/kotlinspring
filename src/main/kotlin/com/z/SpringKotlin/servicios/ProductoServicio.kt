package com.z.SpringKotlin.servicios

import com.z.SpringKotlin.dao.ProductoDAO
import com.z.SpringKotlin.dominio.Producto
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class ProductoServicio(private val productoDAO: ProductoDAO): IBasicoCrud<Producto, String> {
	//Mutable Set para poder cambiar la interfaz
	//private val producto:MutableSet<Producto> = mutableSetOf(Producto("Peluche totoro",499),Producto("Base para monitor",1200), Producto("peluche",449))

	//Implentado la interfaz
	override fun findById(id: String): Producto? {
		/* Otra forma de retornar esto es con «it»
		* return this.producto.find { it.producto == id }
		* */
		//return this.producto.find { producto -> producto.producto  == id }
		return this.productoDAO.findByIdOrNull(id)
	}

	override fun guardar(t: Producto): Boolean {
		//return this.producto.add(t)
		return this.productoDAO.save(t).let { return true }
	}
	//No hay una funcion update asi que hay que emular esto
	override fun actualizar(t: Producto): Boolean {
		//return this.producto.remove(t) && this.producto.add(t)
		this.productoDAO.save(t).let { return true }
	}

	override fun eliminar(id: String): Boolean {
		//return this.producto.remove(this.findById(id))
		return this.productoDAO.deleteById(id).let { return true }
	}

	override fun findAll(): List<Producto> {
		//return producto.toList()
		return this.productoDAO.findAll()
	}
}