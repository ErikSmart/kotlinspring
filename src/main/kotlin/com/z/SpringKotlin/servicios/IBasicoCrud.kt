package com.z.SpringKotlin.servicios

//Creando la interfaz del Crud
interface IBasicoCrud<T,ID>{
	fun findAll():List<T>
	fun  findById(id:ID): T?
	fun guardar(t:T): Boolean
	fun actualizar(t:T): Boolean
	fun eliminar(id:ID):Boolean
}