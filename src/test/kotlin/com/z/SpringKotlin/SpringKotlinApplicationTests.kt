package com.z.SpringKotlin

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.z.SpringKotlin.dominio.Producto
import com.z.SpringKotlin.servicios.ProductoServicio
import org.hamcrest.Matchers
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class SpringKotlinApplicationTests {
	@Autowired
	private lateinit var  mockMvc: MockMvc
	@Autowired
	private lateinit var mapper: ObjectMapper
	@Autowired
	private lateinit var productoServicio: ProductoServicio
	@Test
	fun finAll() {
		val productoFromServicio = productoServicio.findAll()
		val json = mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(status().isOk)
				.andReturn().response.contentAsString
		val productos: List<Producto> = mapper.readValue(json)
		//assertThat(productoFromServicio, Matchers.`is`(Matchers.equalTo(productos)))
	}

}